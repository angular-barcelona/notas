import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingComponent } from './loading/loading.component';
import { ValorEstadoPipe } from './valor-estado.pipe';
import { NotFoundComponent } from './not-found/not-found.component';



@NgModule({
  declarations: [LoadingComponent, ValorEstadoPipe, NotFoundComponent],
  imports: [
    CommonModule
  ],
  exports: [LoadingComponent, ValorEstadoPipe]
})
export class SharedModule { }
