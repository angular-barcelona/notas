import { Pipe, PipeTransform } from '@angular/core';
import { Estados } from '../app.component';

@Pipe({
  name: 'valorEstado'
})
export class ValorEstadoPipe implements PipeTransform {

  transform(estado: Estados): string {
    return Estados[estado];
  }

}
