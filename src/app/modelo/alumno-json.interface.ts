export interface AlumnoJSON {
  id: number;
  aula: string;
  nombre: string;
  apellido: string;
  nota: number;
}
