export interface Alumno {
  id: number;
  aula: string;
  nombre: string;
  apellido: string;
  nota: number;
  nombreLargo: boolean;
}
