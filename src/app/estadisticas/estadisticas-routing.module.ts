import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ResumenAulaComponent } from '../alumnos/resumen-aula/resumen-aula.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';

const rutas: Routes = [{
  path: '',
  component: EstadisticasComponent
}];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class EstadisticasRoutingModule { }
