import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './info/inicio/inicio.component';
import { PantallaAulasComponent } from './alumnos/pantalla-aulas/pantalla-aulas.component';
import { FormAlumnoComponent } from './alumnos/form-alumno/form-alumno.component';
import { AuthGuard } from './core/auth.guard';
import { NotFoundComponent } from './shared/not-found/not-found.component';

const rutas: Routes = [
  {
    path: 'estadisticas',
    loadChildren: () => import('./estadisticas/estadisticas.module').then(modulo => modulo.EstadisticasModule)
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
