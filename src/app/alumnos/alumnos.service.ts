import { Injectable } from '@angular/core';
import { Alumno } from '../modelo/alumno.interface';
import { BehaviorSubject, Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class AlumnosService {

  public aulas = ['A', 'B', 'C'];

  private alumnos = new BehaviorSubject<Alumno[]>([]);
  public alumnos$ = this.alumnos.asObservable();
  private alumno = new BehaviorSubject<Alumno>(null);
  public alumno$ = this.alumno.asObservable();


  constructor(private api: ApiService) {
    this.api.getAlumnos().subscribe(
      alumnos => { this.alumnos.next(alumnos); }
    );
  }

  public borrarAlumno(alumno: Alumno): void {

    this.api.borrarAlumno(alumno).subscribe(
      resp => {
        if (resp) {
          const alumnosActuales = this.alumnos.getValue();
          const alumnosNuevos = alumnosActuales.filter(al => al !== alumno);
          this.alumnos.next(alumnosNuevos);
        }
      }
    );
  }

  public toggleNota(alumno: Alumno): void {
    const alumnosActuales = this.alumnos.getValue();
    const alumnosNuevos = alumnosActuales.map(
      al => {
        if (al === alumno) {
          return {
            ...al,
            nota: al.nota !== 10 ? 10 : 0
          };
        } else {
          return al;
        }
      }
    );
    this.alumnos.next(alumnosNuevos);
  }

  public anyadirAlumno(alumno: Alumno): void {
    this.api.anyadirAlumno(alumno).subscribe(
      al =>
        this.alumnos.next([
          ...this.alumnos.getValue(),
          al
        ])
    );
  }

  public modificarAlumno(alumno: Alumno): void {
    this.api.modificarAlumno(alumno).subscribe(
      () => {
        const nuevaBolita = this.alumnos.getValue().map(
          al => al.id === alumno.id ? alumno : al
        );
        this.alumnos.next(nuevaBolita);
      }
    );
  }

  public getAlumnoPorId(id: number): Observable<Alumno> {
    const alumno = this.alumnos.getValue().find(al => al.id === id);
    this.alumno.next(alumno);
    return this.alumno$;
  }
}
