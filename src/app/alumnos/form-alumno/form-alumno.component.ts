import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AlumnosService } from '../alumnos.service';
import { Alumno } from 'src/app/modelo/alumno.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'notas-form-alumno',
  templateUrl: './form-alumno.component.html',
  styleUrls: ['./form-alumno.component.css']
})
export class FormAlumnoComponent implements OnInit, OnDestroy {

  public alumno: Alumno;
  public aula: string;
  public textoBoton = 'añadir';
  private suscripciones: Subscription[] = [];

  constructor(private alumnosService: AlumnosService, private route: ActivatedRoute, private router: Router) { }

  public enviarAlumno(formulario): void {
    const nuevoAlumno = {
      ...this.alumno,
      nombreLargo: this.alumno.nombre.length + this.alumno.apellido.length >= 15
    };
    if (this.alumno.id === 0) {
      this.alumnosService.anyadirAlumno(nuevoAlumno);
    } else {
      this.alumnosService.modificarAlumno(nuevoAlumno);
    }
    formulario.resetForm();
    this.router.navigate(['/aulas']);
  }

  ngOnInit() {
    const suscripcion = this.route.data.pipe(
      filter(datos => datos.aula !== undefined)
    ).subscribe(
      datos => {
        this.aula = datos.aula;
      }
    );
    const suscripcion2 = this.route.paramMap.pipe(
      filter(parametros => parametros.get('id') !== undefined)
    ).subscribe(
      parametros => {
        this.alumnosService.getAlumnoPorId(+parametros.get('id')).subscribe(
          alumno => this.alumno = alumno
        );
      }
    );
    this.suscripciones.push(suscripcion, suscripcion2);
    if (this.alumno === undefined) {
      this.alumno = {
        id: 0,
        aula: this.aula,
        nombre: '',
        apellido: '',
        nota: null,
        nombreLargo: false
      };
    } else {
      this.alumno = {
        ...this.alumno
      };
      this.textoBoton = 'modificar';
    }
  }

  ngOnDestroy() {
    for (const suscripcion of this.suscripciones) {
      suscripcion.unsubscribe();
    }
  }
}
