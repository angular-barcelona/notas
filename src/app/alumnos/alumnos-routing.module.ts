import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PantallaAulasComponent } from './pantalla-aulas/pantalla-aulas.component';
import { FormAlumnoComponent } from './form-alumno/form-alumno.component';
import { AuthGuard } from '../core/auth.guard';

const rutas: Routes = [
  {
    path: 'aulas',
    component: PantallaAulasComponent
  },
  {
    path: 'nuevo-alumno',
    component: FormAlumnoComponent,
    canActivate: [AuthGuard],
    data: {
      aula: 'A'
    }
  },
  {
    path: 'editar-alumno/:id',
    component: FormAlumnoComponent
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(rutas)
  ],
  exports: [
    RouterModule
  ]
})
export class AlumnosRoutingModule { }
