import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AulaComponent } from './aula/aula.component';
import { ListaAlumnosComponent } from './lista-alumnos/lista-alumnos.component';
import { ResumenAulaComponent } from './resumen-aula/resumen-aula.component';
import { FormAlumnoComponent } from './form-alumno/form-alumno.component';
import { PantallaAulasComponent } from './pantalla-aulas/pantalla-aulas.component';
import { RouterModule } from '@angular/router';
import { AlumnosRoutingModule } from './alumnos-routing.module';



@NgModule({
  declarations: [AulaComponent, ListaAlumnosComponent, ResumenAulaComponent, FormAlumnoComponent, PantallaAulasComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    AlumnosRoutingModule
  ],
  exports: [AulaComponent, FormAlumnoComponent]
})
export class AlumnosModule { }
