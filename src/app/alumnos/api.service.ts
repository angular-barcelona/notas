import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { AlumnoJSON } from '../modelo/alumno-json.interface';
import { map, delay, retry, catchError } from 'rxjs/operators';
import { Alumno } from '../modelo/alumno.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  public getAlumnos(): Observable<Alumno[]> {
    return this.http.get<AlumnoJSON[]>(environment.urlApi).pipe(
      retry(2),
      map(alumnosJSON => alumnosJSON.map(
        alumnoJSON =>
          ({
            ...alumnoJSON,
            nota: alumnoJSON.nota < 0 ? 0 : alumnoJSON.nota > 10 ? 10 : alumnoJSON.nota,
            nombreLargo: alumnoJSON.nombre.length + alumnoJSON.apellido.length > 15
          }))
      ));
  }

  public borrarAlumno(alumno: Alumno): Observable<boolean> {
    return this.http.delete<HttpResponse<any>>(environment.urlApi + '/' + alumno.id, { observe: 'response' }).pipe(
      map(resp => resp.ok)
    );
  }

  public anyadirAlumno(alumno: Alumno): Observable<Alumno> {
    return this.http.post<Alumno>(environment.urlApi, alumno);
  }

  public modificarAlumno(alumno: Alumno): Observable<Alumno> {
    return this.http.put<Alumno>(environment.urlApi + '/' + alumno.id, alumno);
  }
}
