import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Alumno } from 'src/app/modelo/alumno.interface';
import { AlumnosService } from '../alumnos.service';

@Component({
  selector: 'notas-resumen-aula',
  templateUrl: './resumen-aula.component.html',
  styleUrls: ['./resumen-aula.component.css']
})
export class ResumenAulaComponent implements OnInit, OnChanges {

  @Input()
  public aula: string;

  @Input()
  public alumnos: Alumno[];

  public notaMedia: number;
  public notaMaxima: number;
  public aprobados = {
    total: 0,
    porcentaje: 0
  };
  public suspensos = {
    total: 0,
    porcentaje: 0
  };

  constructor(private alumnosService: AlumnosService) { }

  private actualizar(): void {
    this.calcularMedia();
    this.calcularMaxima();
    this.calcularAprobadosSuspensos();
  }

  private calcularMedia(): void {
    this.notaMedia = this.alumnos.reduce((acc, alumno) => acc + alumno.nota / this.alumnos.length, 0);
  }

  private calcularMaxima(): void {
    this.notaMaxima = this.alumnos.reduce((max, alumno) => max > alumno.nota ? max : alumno.nota, 0);
  }

  private calcularAprobadosSuspensos(): void {
    const aprobados = this.alumnos.filter(alumno => alumno.nota >= 5);
    this.aprobados.total = aprobados.length;
    this.aprobados.porcentaje = (aprobados.length / this.alumnos.length) * 100;
    this.suspensos.total = this.alumnos.length - aprobados.length;
    this.suspensos.porcentaje = 100 - this.aprobados.porcentaje;
  }

  public getClasesCSS(n: number) {
    return {
      'fa-smile': n >= 5,
      'fa-frown': n < 5,
      'icono-ok': n >= 5,
      'icono-no-ok': n < 5
    };
  }

  ngOnChanges() {
    this.actualizar();
  }

  ngOnInit() {
    this.actualizar();
  }

}
