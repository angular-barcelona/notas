import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authToken = localStorage.getItem('token');
    let peticion = req.clone();

    if (peticion.url.includes(environment.urlApi)) {
      peticion = req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + authToken
        }
      });
      peticion.headers.set('Authorization', 'Bearer ' + authToken);
      return next.handle(peticion).pipe(
        catchError(err => {
          console.log('Error al conectar a la API');
          return throwError(err);
        })
      );
    } else {
      return next.handle(peticion);
    }
  }

  constructor() { }
}
