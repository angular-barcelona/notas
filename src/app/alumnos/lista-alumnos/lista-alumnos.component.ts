import { Component, OnInit, Input } from '@angular/core';
import { Alumno } from 'src/app/modelo/alumno.interface';
import { AlumnosService } from '../alumnos.service';
import { MensajeroService } from 'src/app/core/mensajero.service';
import { TemasMensajes } from 'src/app/core/temas-mensajes.enum';
import { TextosMensajes } from 'src/app/core/textos-mensajes.enum';

@Component({
  selector: 'notas-lista-alumnos',
  templateUrl: './lista-alumnos.component.html',
  styleUrls: ['./lista-alumnos.component.css']
})
export class ListaAlumnosComponent implements OnInit {

  @Input()
  public aula: string;

  @Input()
  public alumnos: Array<Alumno>;

  constructor(private alumnosService: AlumnosService, private mensajero: MensajeroService) { }

  public borrarAlumno(evento: Event, alumno: Alumno): void {
    evento.preventDefault();
    this.alumnosService.borrarAlumno(alumno);
  }

  public toggleNota(alumno: Alumno): void {
    this.alumnosService.toggleNota(alumno);
  }

  public abrirFormulario(alumno: Alumno): void {
    this.mensajero.emitir({
      tema: TemasMensajes.General,
      texto: TextosMensajes.GeneralAbrirFormulario,
      datos: {
        alumno
      }
    });
  }

  ngOnInit() {

  }

}
