import { Component, OnInit } from '@angular/core';
import { AlumnosService } from '../alumnos.service';

@Component({
  selector: 'notas-pantalla-aulas',
  templateUrl: './pantalla-aulas.component.html',
  styleUrls: ['./pantalla-aulas.component.css']
})
export class PantallaAulasComponent implements OnInit {

  public aulas: string[] = [];

  constructor(private alumnosService: AlumnosService) { }

  ngOnInit() {
    this.aulas = this.alumnosService.aulas;
  }

}
