import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { map } from 'rxjs/operators';

import { Alumno } from 'src/app/modelo/alumno.interface';
import { AlumnosService } from '../alumnos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'notas-aula',
  templateUrl: './aula.component.html',
  styleUrls: ['./aula.component.css']
})
export class AulaComponent implements OnInit {

  @Input()
  public aula: string;

  public alumnos: Alumno[];

  constructor(private alumnosService: AlumnosService, private router: Router) { }

  public anyadirAlumno(evento: Event): void {
    evento.preventDefault();
    this.alumnosService.anyadirAlumno({
      id: 10,
      aula: this.aula,
      nombre: 'Tobías',
      apellido: 'Tobías',
      nota: 2,
      nombreLargo: false
    });
  }

  public abrirForm(evento: Event): void {
    evento.preventDefault();
    this.router.navigate(['/nuevo-alumno']);
  }

  ngOnInit() {
    this.alumnosService.alumnos$.pipe(
      map(
        alumnos => alumnos.filter(alumno => alumno.aula.toLowerCase() === this.aula.toLowerCase())
      )
    ).subscribe(
      alumnos => this.alumnos = alumnos
    );
  }

}
