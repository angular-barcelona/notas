import { TemasMensajes } from './temas-mensajes.enum';
import { TextosMensajes } from './textos-mensajes.enum';

export interface Mensaje {
  tema: TemasMensajes;
  texto: TextosMensajes;
  datos: any;
}
