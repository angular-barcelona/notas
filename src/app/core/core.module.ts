import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [CabeceraComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [CabeceraComponent]
})
export class CoreModule { }
