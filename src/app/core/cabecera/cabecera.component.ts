import { Component, OnInit } from '@angular/core';
import { AlumnosService } from 'src/app/alumnos/alumnos.service';

@Component({
  selector: 'notas-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {

  public totalNotas: number;

  constructor(private alumnoService: AlumnosService) { }

  ngOnInit() {
    this.alumnoService.alumnos$.subscribe(
      alumnos => this.totalNotas = alumnos.length
    );
  }

}
