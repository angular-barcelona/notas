import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Mensaje } from './mensaje.interface';

@Injectable({
  providedIn: 'root'
})
export class MensajeroService {

  private mensajero = new BehaviorSubject<Mensaje>({
    tema: null,
    texto: null,
    datos: null
  });

  constructor() { }

  public emitir(mensaje: Mensaje): void {
    this.mensajero.next(mensaje);
  }

  public escuchar(): Observable<Mensaje> {
    return this.mensajero.asObservable();
  }
}
