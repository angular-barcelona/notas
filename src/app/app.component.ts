import { Component, OnInit } from '@angular/core';
import { MensajeroService } from './core/mensajero.service';
import { TemasMensajes } from './core/temas-mensajes.enum';
import { TextosMensajes } from './core/textos-mensajes.enum';
import { filter } from 'rxjs/operators';
import { Alumno } from './modelo/alumno.interface';

export enum Estados { Aprobado, Suspenso }

@Component({
  selector: 'notas-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() { }

}
